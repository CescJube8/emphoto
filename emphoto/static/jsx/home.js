/**
 * Created by fcaspani on 23/08/16.
 */

// tutorial1.js
var Home = React.createClass({
  render: function() {
    return (
      <div className="commentBox">
        Hello, world! I am a CommentBox.
      </div>
    );
  }
});


ReactDOM.render(
    <Home />,
    document.getElementById('content')
);
